const jwt = require('jsonwebtoken');
const { JWT_SECRET } = require('../env');

module.exports = (req, res, next) => {
  try {
    const token = req.headers.authorization.split(" ")[1];
    const decodedToken = jwt.verify(token, JWT_SECRET);
    req.userData = decodedToken;
    next();
  } catch (err) {
    console.log("Still not match!");
    return res.status(401).json({
      success: false,
      message: `Missing authorization!`,
    });
  }
};