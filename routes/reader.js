const express = require('express');
const router = express.Router();
const JSONStream = require('JSONStream');

const mongoose = require('mongoose');
const checkAuth = require('../middleware/checkAuth');
const { checkForRequired } = require('../utils');

const PostMessage = require('../schemas/reader/postMessage');

const {
  ERROR_NO_AUTHENTICATION,
} = require('../env');

router.get('/post-messages', (req, res, next) => {
  PostMessage
    .find({ })
    .cursor()
    .pipe(JSONStream.stringify())
    .pipe(res);
});

router.get('/post-message/:pmid', checkAuth, (req, res, next) => {
  const { pmid } = req.params;

  PostMessage
    .find({
      _id: mongoose.Types.ObjectId(pmid),
    })
    .then(
      _post => {
        if (!_post) {
          return res.status(400).json({
            success: false,
            error: "Post does not exist",
            post: null,
          });
        }

        return res.status(200).json({
          success: true,
          error: null,
          post: _post,
        });
      }
    )
    .catch(
      error => {
        console.log(`Error when fetching post: ${error}`);
        return res.status(500).json({
          success: false,
          error,
          post: null,
        });
      }
    );
});

router.post('/post-message', checkAuth, (req, res, next) => {
  const postMessageInfo = req.body;
  const { username } = req.userData;

  const requiredKeys = ['groupId', 'postId', 'message', 'timestamp'];
  const missingKeys = checkForRequired(postMessageInfo, requiredKeys);
  if (missingKeys.length) {
    return res.status(400).json({
      success: false,
      error: `Missing ${JSON.stringify(missingKeys)}`,
      post: null,
    });
  }

  const {
    origin,
    translationOrigin,
    translator,
    groupId,
    postId,
    format,
    category,
    collection,
    message,
    images,
    tags,
    remainingImages,
    timestamp,
    timestampUTC,
    textContent,
    imageContent,
  } = postMessageInfo;

  const postMessage = new PostMessage({
    _id: `${groupId}_${postId}`,
    origin,
    translationOrigin,
    translator,
    facebookGroupId: groupId,
    facebookPostId: postId,
    format,
    category,
    _collection: collection,
    message,
    images: images ? images.map(_i => ({ src: _i.dataSrc, alt: _i.alt })) : [],
    tags,
    remainingImages,
    timestamp,
    timestampUTC,
    textContent,
    imageContent,
    contributor: username,
  });

  postMessage.save()
    .then(
      _post => {
        if (!_post) {
          return res.status(400).json({
            success: false,
            error: _post,
            post: null,
          });
        }

        return res.status(201).json({
          success: true,
          error: null,
          post: _post,
        });
      }
    )
    .catch(
      error => {
        console.log(`Error when creating spec: ${error}`);
        return res.status(500).json({
          success: false,
          error,
          spec: null,
        });
      }
    );
});

router.delete('/post-message/:pmid', checkAuth, (req, res, next) => {
  const { pmid } = req.params;

  PostMessage.deleteOne(
    {
      _id: mongoose.Types.ObjectId(pmid),
    })
    .then(
      result => {
        if (!result || result.n === 0) {
          return res.status(400).json({
            success: false,
            error: "Post does not exist",
            post: null,
          });
        }

        return res.status(200).json({
          success: true,
          error: null,
          post: pmid,
        });
      }
    )
    .catch(
      error => {
        console.log(`Error when deleting post: ${error}`);
        return res.status(500).json({
          success: false,
          error,
          post: null,
        });
      }
    );
});

router.get('/random/post-message', checkAuth, (req, res, next) => {
  PostMessage
    .aggregate([
      {
        $sample: { size: 1 },
      },
    ])
    .then(
      result => {
        console.log(result);
      }
    )
    .catch(
      error => {
        console.log(error);
      }
    );

    return res.status(200).json({
      success: true,
      error: null,
    });
});

module.exports = router;