const express = require('express');
const router = express.Router();
const JSONStream = require('JSONStream');

const mongoose = require('mongoose');
const checkAuth = require('../middleware/checkAuth');
const { checkForRequired, checkForLeastRequired } = require('../utils');

const EnglishWord = require('../schemas/fragment/wordEnglish');
const EnglishPhrase = require('../schemas/fragment/phraseEnglish');

router.get('/english/words', (req, res, next) => {
  EnglishWord
    .find({ })
    .cursor()
    .pipe(JSONStream.stringify())
    .pipe(res);
});

router.get('/english/phrases', (req, res, next) => {
  EnglishPhrase
    .find({ })
    .cursor()
    .pipe(JSONStream.stringify())
    .pipe(res);
});

router.post('/english/word', checkAuth, (req, res, next) => {
  const wordInfo = req.body;
  const { username } = req.userData;

  const requiredKeys = ['word', 'wordType', 'pronunciation', 'meaning'];
  const missingKeys = checkForRequired(wordInfo, requiredKeys);
  if (missingKeys.length) {
    return res.status(400).json({
      success: false,
      error: `Missing ${JSON.stringify(missingKeys)}`,
      word: null,
    });
  }

  const {
    wordType,
    word,
    pronunciation,
    meaningSource,
    meaning,
    examples,
    relatives,
    groups,
    alternate,
    origin,
    variant,
  } = wordInfo;

  const englishWord = new EnglishWord({
    wordType,
    word,
    pronunciation,
    meaningSource,
    meaning,
    examples,
    relatives,
    groups,
    alternate,
    origin,
    variant,
  });

  englishWord.save()
    .then(
      _word => {
        if (!_word) {
          return res.status(400).json({
            success: false,
            error: `Can not create word ${word}`,
            word: null,
          });
        }

        return res.status(201).json({
          success: true,
          error: null,
          word: _word,
        });
      }
    )
    .catch(
      error => {
        console.log(`Error when creating word: ${error}`);
        return res.status(500).json({
          success: false,
          error,
          word: null,
        });
      }
    );
});

router.post('/english/word/:wid', checkAuth, (req, res, next) => {
  const wordInfo = req.body;
  const { username } = req.userData;
  const { wid } = req.params;

  if (!wordInfo._id || wordInfo._id !== wid) {
    return res.status(400).json({
      success: false,
      error: `No specific word provided`,
      word: null,
    });
  }

  const requiredKeys = ['wordType', 'pronunciation', 'meaningSource', 'meaning', 'examples', 'relatives', 'groups', 'alternate', 'origin'];
  const valid = checkForLeastRequired(wordInfo, requiredKeys);
  if (!valid) {
    return res.status(400).json({
      success: false,
      error: `At least one required: ${requiredKeys.join(', ')}`,
      word: null,
    });
  }

  EnglishWord.updateOne(
    {
      _id: wid,
    },
    {
      ...wordInfo,
    })
    .then(
      result => {
        if (result.nModified === 0) {
          return res.status(400).json({
            success: false,
            error: `No changes made for word ${wid}`,
            word: null,
          });
        }

        return res.status(200).json({
          success: true,
          error: null,
          word: wordInfo,
        });
      }
    )
    .catch(
      error => {
        console.log(`Error when updating word ${wid}: ${error}`);
        return res.status(500).json({
          success: false,
          error,
          word: null,
        });
      }
    );
});

module.exports = router;