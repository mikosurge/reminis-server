const express = require('express');
const router = express.Router();

const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const mongoose = require('mongoose');

const checkAuth = require('../middleware/checkAuth');

const User = require('../schemas/account/user');
const Collection = require('../schemas/mtg/mtgCollection');
const { checkForRequired } = require('../utils');
const {
  AUTHCODE,
  DBCODE,
  AuthenticationError,
  DatabaseError,
} = require('../utils/error');

const { JWT_SECRET } = require('../env');
const SALT_LENGTH = 10;

router.get('/unlock', checkAuth, (req, res, next) => {
  const { userId, username, email } = req.userData;
  const token = req.headers.authorization.split(" ")[1];
  User.findOne(
    {
      _id: mongoose.Types.ObjectId(userId),
      username: username,
      email: email
    })
    .then(
      user => {
        if (!user) {
          throw new AuthenticationError(AUTHCODE.WRONG_USERNAME);
        }

        return res.status(200).json({
          success: true,
          user,
          token,
          expiresIn: 7200,
          error: null,
        });
      }
    )
    .catch(
      error => {
        if (error instanceof AuthenticationError) {
          return res.status(400).json({
            success: false,
            error: error.message,
            user: null,
          })
        } else {
          console.log(error);
          return res.status(500).json({
            success: false,
            error: error.message,
            user: null,
          });
        }
      }
    );
});

const createCollectionAndUpdateUser = async (_user) => {
  // Create a collection and link to the user
  const userCollection = new Collection({
    name: `${_user.realName}'s Collection`,
    description: `${_user.realName}'s Collection`,
    owner: mongoose.Types.ObjectId(_user._id),
    cards: [],
    createdAt: new Date().toISOString(),
  });

  const _collection = await userCollection.save();
  await User.updateOne(
    {
      _id: mongoose.Types.ObjectId(_user._id),
    },
    {
      "mtg.collection": mongoose.Types.ObjectId(_collection._id),
    },
  );

  _user.mtg.collection = mongoose.Types.ObjectId(_collection._id);

  return _user;
};

router.post('/signup', (req, res, next) => {
  const profile = req.body;

  // Simple check for legality
  const requiredFields = ['realName', 'username', 'password', 'email'];
  const missingKeys = checkForRequired(profile, requiredFields);
  if (missingKeys.length) {
    return res.status(400).json({
      success: false,
      error: `Missing ${JSON.stringify(missingKeys)}`,
      user: null,
    });
  }

  let token = null;

  bcrypt.hash(profile.password, SALT_LENGTH)
    .then(
      hash => {
        const { realName, username, email, nickname, biography } = profile;
        const user = new User({
          realName,
          username,
          email,
          password: hash,
          nickname,
          biography,
          createdAt: new Date().toISOString(),
          updatedAt: new Date().toISOString(),
          mtg: {
            collection: null,
            decks: []
          },
        });

        return user.save();
      }
    )
    .then(
      result => {
        token = jwt.sign(
          { userId: result._id, username: result.username, email: result.email },
          JWT_SECRET,
          { expiresIn: '2h' }
        );

        return createCollectionAndUpdateUser(result);
      }
    )
    .then(
      updatedUser => {
        if (!updatedUser) {
          throw new DatabaseError(DBCODE.OPERATION_FAILED, "Create collection and update user");
        }

        return res.status(201).json({
          success: true,
          user: updatedUser,
          error: null,
          token: token,
          expiresIn: 7200,
        });
      }
    )
    .catch(
      error => {
        if (error instanceof DatabaseError) {
          return res.status(400).json({
            success: false,
            error: error.message,
            user: null,
          });
        } else if (error instanceof MongoError) {
          if (error.code === 11000) {
            return res.status(400).json({
              success: false,
              error: "User already exists",
              user: null,
            });
          } else {
            console.log(error);
            return res.status(500).json({
              success: false,
              error: "Unexpected error",
              user: null,
            });
          }
        } else {
            console.log(error);
            return res.status(500).json({
            success: false,
            error: "Unexpected error",
            user: null,
          });
        }
      }
    );
});

router.post('/login', (req, res, next) => {
  let fetchedUser;
  console.log(`Attempt to login from user ${req.body.username}`);
  User.findOne({ username: req.body.username })
    .then(
      user => {
        if (!user) {
          throw new AuthenticationError(AUTHCODE.WRONG_USERNAME);
        }
        fetchedUser = user;
        return bcrypt.compare(req.body.password, user.password);
      }
    )
    .then(
      result => {
        if (!result) {
          throw new AuthenticationError(AUTHCODE.WRONG_PASSWORD);
        }

        const { _id, realName, username, email, nickname, biography, mtg, avatar } = fetchedUser;
        const token = jwt.sign(
          { userId: fetchedUser._id, username: fetchedUser.username, email: fetchedUser.email },
          JWT_SECRET,
          { expiresIn: '2h' }
        );

        return res.status(200).json({
          success: true,
          user: {
            _id,
            realName,
            username,
            email,
            nickname,
            biography,
            avatar: avatar || '',
            mtg,
          },
          token: token,
          expiresIn: 7200,
        });
      }
    )
    .catch(
      error => {
        if (error instanceof AuthenticationError) {
          return res.status(401).json({
            success: false,
            error: error.message,
            user: null,
          });
        } else {
          console.log(error);
          return res.status(500).json({
            success: false,
            error: error.message,
            user: null,
          });
        }
      }
    );
});

router.get('/logout', checkAuth, (req, res, next) => {
  res.status(200).json({
    success: true
  });
});

module.exports = router;