const express = require('express');
const router = express.Router();
const axios = require('axios');
const fs = require('fs');
const JSONStream = require('JSONStream');
const { Timer, checkForRequired } = require('../utils');

const mongoose = require('mongoose');
const User = require('../schemas/account/user');
const Card = require('../schemas/mtg/mtgCard');
const Collection = require('../schemas/mtg/mtgCollection');
const Deck = require('../schemas/mtg/mtgDeck');

const checkAuth = require('../middleware/checkAuth');
const {
  ERROR_NO_AUTHENTICATION
} = require('../env');

const {
  getAllGroupsAsync,
  getGroupPriceAndUpdateDatabaseAsync,

} = require('../lib/mtg');


router.get('/update/tcgplayer/groups', (req, res, next) => {
  getAllGroupsAsync()
    .then(
      logs => {
        res.json({
          success: true,
          error: null,
          logs,
        });
      }
    )
    .catch(
      error => {
        res.json({
          success: false,
          error,
          logs: null,
        });
      }
    );
});


router.get('/update/tcgplayer/pricing/:groupId', (req, res, next) => {
  getGroupPriceAndUpdateDatabaseAsync(req.params.groupId)
    .then(
      logs => {
        res.json({
          success: true,
          error: null,
          logs,
        });
      }
    )
    .catch(
      error => {
        res.json({
          success: false,
          error,
          logs: null,
        });
      }
    );
});


router.get('/cards/brief', (req, res, next) => {
  Card.find({ },
    {
      _id: 1,
      name: 1,
      manaCost: 1,
      set: 1,
      setName: 1,
      collectorNumber: 1,
      rarity: 1,
      typeLine: 1,
      colorIdentity: 1,
      tcgplayerId: 1,
      tcgPrices: 1,
      cmc: 1,
      colors: 1,
    })
    .stream()
    .pipe(JSONStream.stringify())
    .pipe(res);
});


router.get('/:id/collection/info/:cid', checkAuth, (req, res, next) => {
  const { userId } = req.userData;
  const { id, cid } = req.params;

  if (id !== userId) {
    return res.status(401).json({
      success: false,
      error: ERROR_NO_AUTHENTICATION,
      collection: null,
    });
  }

  Collection
    .findOne({ _id: mongoose.Types.ObjectId(cid) })
    .populate("cards.card", {
      _id: 1,
      name: 1,
      manaCost: 1,
      set: 1,
      setName: 1,
      cmc: 1,
      colors: 1,
      collectorNumber: 1,
      rarity: 1,
      typeLine: 1,
      colorIdentity: 1,
      tcgProductId: 1,
      tcgPrices: 1,
      imageUris: 1,
      cardFaces: 1,
      releasedAt: 1,
    })
    .then(
      collection => {
        return res.status(200).json({
          success: true,
          error: null,
          collection,
        })
      }
    )
    .catch(
      error => {
        return res.status(400).json({
          success: false,
          collection: null,
          error,
        });
      }
    );
});


router.post('/:id/collection/modify/:cid', checkAuth, (req, res, next) => {
  const { userId } = req.userData;
  const { id, cid } = req.params;

  if (id !== userId) {
    return res.status(401).json({
      success: false,
      error: ERROR_NO_AUTHENTICATION,
      modified: null,
    });
  }

  const numberOfProcessingCards = req.body.length;
  const modifyingCards = req.body;
  let numberOfProcessed = 0;
  let logs = [];

  modifyingCards.forEach(
    item => {
      let selectQueries = null;
      let performQueries = null;
      if (item.perform === "insert") {
        selectQueries = {
          _id: mongoose.Types.ObjectId(cid),
        };

        performQueries = {
          $push: {
            cards: {
              card: item.card._id,
              foil: item.foil,
              num: item.num,
            }
          }
        };
      } else if (item.perform === "update") {
        selectQueries = {
          _id: mongoose.Types.ObjectId(cid),
          cards: {
            $elemMatch: {
              card: item.card._id,
              foil: item.foil,
            }
          }
        };

        performQueries = {
          $inc: {
            "cards.$.num": item.num,
          }
        };
      } else if (item.perform === "remove") {
        selectQueries = {
          _id: mongoose.Types.ObjectId(cid),
        };

        performQueries = {
          $pull: {
            cards: {
              card: item.card._id,
              foil: item.foil,
            }
          }
        };
      }

      if (selectQueries && performQueries) {
        Collection.updateOne(selectQueries, performQueries)
          .then(
            res => console.log(`Done ${item.perform} ${item.num} item(s) ${item.card.name}, with res = ${JSON.stringify(res)}`)
          )
          .catch(
            err => logs.push(`Error when ${item.perform} ${item.num} item(s) ${item.card.name}: ${JSON.stringify(err)}`)
          )
          .finally(
            () => {
              numberOfProcessed += 1;
              if (numberOfProcessed === numberOfProcessingCards) {
                return res.json({
                  success: logs.length ? false : true,
                  modified: numberOfProcessed,
                  logs,
                });
              }
            }
          );
      } else {
        numberOfProcessed += 1;
        if (numberOfProcessed === numberOfProcessingCards) {
          return res.json({
            success: logs.length ? false : true,
            modified: processed,
            logs,
          });
        }
      }
    }
  );
});


router.post('/:id/deck/create', checkAuth, (req, res, next) => {
  const { userId } = req.userData;
  const { id } = req.params;
  const deckInfo = req.body;

  if (id !== userId) {
    return res.status(401).json({
      success: false,
      error: ERROR_NO_AUTHENTICATION,
      deck: null,
    });
  }

  const requiredKeys = ["name", "description", "format"];
  const missingKeys = checkForRequired(deckInfo, requiredKeys);
  if (missingKeys.length) {
    return res.status(400).json({
      success: false,
      error: `Missing ${JSON.stringify(missingKeys)}`,
      deck: null,
    });
  }

  const { name, description, avatar, owner, format, colors, imaginary } = deckInfo;
  const deck = new Deck({
    name,
    description,
    avatar,
    owner,
    format,
    colors,
    imaginary,
    updatedAt: new Date().toISOString(),
  });

  deck.save()
    .then(
      _deck => {
        return User.updateOne(
          {
            _id: mongoose.Types.ObjectId(userId),
          },
          {
            $addToSet: {
              "mtg.decks": mongoose.Types.ObjectId(deck._id),
            }
          }
        );
      }
    )
    .then(
      _user => {
        console.log(`Updated user ${userId} with the new deck ${deck._id}`);
        return res.status(201).json({
          success: true,
          error: null,
          deck,
        });
      }
    )
    .catch(
      error => {
        console.log(`Error when updating ${userId} with the new deck ${deck._id}: ${error}`);
        return res.status(400).json({
          success: false,
          error: error.code === 11000 ? "Deck already exists" : error,
          deck: null,
        });
      }
    );
});


router.get('/:id/deck/info/:did', checkAuth, (req, res, next) => {
  const { userId } = req.userData;
  const { id, did } = req.params;

  if (id !== userId) {
    return res.status(401).json({
      success: false,
      error: ERROR_NO_AUTHENTICATION,
      deck: null,
    });
  }

  Deck.findOne(
    {
      _id: mongoose.Types.ObjectId(did),
      owner: mongoose.Types.ObjectId(userId),
    })
    .populate("cards.card", {
      _id: 1,
      name: 1,
      manaCost: 1,
      set: 1,
      setName: 1,
      collectorNumber: 1,
      rarity: 1,
      typeLine: 1,
      colorIdentity: 1,
      tcgProductId: 1,
      tcgPrices: 1,
      imageUris: 1,
      cardFaces: 1,
      cmc: 1,
      colors: 1,
    })
    .then(
      deck => {
        if (!deck) {
          return res.status(400).json({
            success: false,
            error: "Deck not found",
            deck: null,
          });
        }

        return res.status(200).json({
          success: true,
          error: null,
          deck,
        });
      }
    )
    .catch(
      error => {
        return res.status(500).json({
          success: false,
          error,
          deck: null,
        });
      }
    );
});


router.post('/:id/deck/modify/:did', checkAuth, (req, res, next) => {
  const { userId } = req.userData;
  const { id, did } = req.params;

  if (id !== userId) {
    return res.status(401).json({
      success: false,
      error: ERROR_NO_AUTHENTICATION,
      modified: null,
    });
  }

  const numberOfProcessingCards = req.body.length;
  const modifyingCards = req.body;
  let numberOfProcessed = 0;
  let logs = [];

  modifyingCards.forEach(
    item => {
      let selectQueries = null;
      let performQueries = null;
      if (item.perform === "insert") {
        selectQueries = {
          _id: mongoose.Types.ObjectId(did),
        };

        performQueries = {
          $push: {
            cards: {
              card: item.card._id,
              foil: item.foil,
              num: item.num,
              isMain: item.isMain,
            }
          }
        };
      } else if (item.perform === "update") {
        selectQueries = {
          _id: mongoose.Types.ObjectId(did),
          cards: {
            $elemMatch: {
              card: item.card._id,
              foil: item.foil,
              isMain: item.isMain,
            }
          }
        };

        performQueries = {
          $inc: {
            "cards.$.num": item.num,
          }
        };
      } else if (item.perform === "remove") {
        selectQueries = {
          _id: mongoose.Types.ObjectId(did),
        };

        performQueries = {
          $pull: {
            cards: {
              card: item.card._id,
              foil: item.foil,
              isMain: item.isMain,
            }
          }
        };
      }

      if (selectQueries && performQueries) {
        Deck.updateOne(selectQueries, performQueries)
          .then(
            res => console.log(`Done ${item.perform} ${item.num} item(s) ${item.card.name}, with res = ${JSON.stringify(res)}`)
          )
          .catch(
            err => logs.push(`Error when ${item.perform} ${item.num} item(s) ${item.card.name}: ${JSON.stringify(err)}`)
          )
          .finally(
            () => {
              numberOfProcessed += 1;
              if (numberOfProcessed === numberOfProcessingCards) {
                return res.json({
                  success: logs.length ? false : true,
                  modified: numberOfProcessed,
                  logs,
                });
              }
            }
          );
      } else {
        numberOfProcessed += 1;
        if (numberOfProcessed === numberOfProcessingCards) {
          return res.json({
            success: logs.length ? false : true,
            modified: numberOfProcessed,
            logs,
          });
        }
      }
    }
  );
});


router.get('/:id/decks', checkAuth, (req, res, next) => {
  const { userId } = req.userData;

  User.findOne(
    {
      _id: mongoose.Types.ObjectId(userId),
    })
    .populate("mtg.decks", {
      _id: 1,
      name: 1,
      description: 1,
      avatar: 1,
      colors: 1,
      format: 1,
      visited: 1,
      updatedAt: 1,
      imaginary: 1,
      releasedAt: 1,
    })
    .then(
      _user => {
        if (!_user) {
          return res.status(400).json({
            success: false,
            error: "User not found",
            decks: null,
          });
        }

        return res.status(200).json({
          success: true,
          error: null,
          decks: _user.mtg.decks,
        })
      }
    )
    .catch(
      error => {
        return res.status(500).json({
          success: false,
          error,
          decks: null,
        })
      }
    );
});


router.delete('/:id/deck/delete/:did', checkAuth, (req, res, next) => {
  const { userId } = req.userData;
  const { id, did } = req.params;

  if (id !== userId) {
    return res.status(401).json({
      success: false,
      error: ERROR_NO_AUTHENTICATION,
      deck: null,
    });
  }

  Deck.deleteOne(
    {
      _id: mongoose.Types.ObjectId(did),
      owner: mongoose.Types.ObjectId(userId),
    })
    .then(
      result => {
        if (!result || result.n === 0) {
          return res.status(400).json({
            success: false,
            error: "Deck does not exist",
            deck: null,
          });
        }

        return User.updateOne(
          {
            _id: mongoose.Types.ObjectId(userId),
          },
          {
            $pull: {
              "mtg.decks": mongoose.Types.ObjectId(did),
            }
          }
        )
      }
    )
    .then(
      result => {
        return res.status(200).json({
          success: true,
          error: null,
          deck: did,
        });
      }
    )
    .catch(
      error => {
        return res.status(500).json({
          success: false,
          error,
          deck: did,
        });
      }
    );
});


module.exports = router;