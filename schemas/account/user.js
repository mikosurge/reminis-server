const mongoose = require('mongoose');
const Schema = mongoose.Schema;

/**
 * @description Formal user schema
 * 'sparse' is added here to indicate the field is unique, but ignore the 'null' case
 * @class userSchema
 * @param {String}            realName - The real user name
 * @param {String}            username - The name with no space, used as an ID for URL
 * @param {String}            ldapName - The unique which refers to LDAP authentication
 * @param {String}            googleId - The google id which represents the user, via OAuth2
 * @param {String}               email - The user email
 * @param {String}            password - The user password after being hash processed by an algorithm
 * @param {String}            nickname - The user nickname
 * @param {String}              gender - The user gender
 * @param {Date}             createdAt - The time this entry is created
 * @param {Date}             updatedAt - The last time this entry is updated
 * @param {Date}             lastLogin - The last time the user logged in
 */
const userSchema = new Schema({
  realName: { type: String, required: true },
  username: { type: String, index: true, required: true, unique: true, trim: true },
  ldapId: { type: String, index: true, unique: true, sparse: true },
  googleId: { type: String, index: true, unique: true, sparse: true },
  avatar: { type: String, default: ''},
  email: { type: String, required: true },
  password: { type: String, default: '' },
  nickname: { type: String },
  origin: { type: String, enum: ['direct', 'ldap', 'google']},
  biography: { type: String, default: "" },
  createdAt: Date,
  updatedAt: Date,
  lastLogin: Date,
  mtg: {
    collection: { type: mongoose.Schema.Types.ObjectId, ref: 'Collection' },
    decks: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Deck' }],
  }
});

module.exports = mongoose.model('User', userSchema);