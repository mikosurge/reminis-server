const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const {
  FORMAT,
  CATEGORY,
} = require('./readerEnum');


const postMessageSchema = new Schema({
  _id: { type: String, requied: true },
  origin: { type: String },
  translationOrigin: { type: String },
  translator: { type: String },
  facebookGroupId: { type: Number, required: true },
  facebookPostId: { type: Number, required: true },
  format: { type: String, enum: FORMAT, default: 'md'},
  category: { type: String, enum: CATEGORY },
  _collection: { type: String },
  message: { type: String, required: true },
  images: [{
    src: { type: String },
    alt: { type: String },
  }],
  tags: [
    { type: String }
  ],
  remainingImages: { type: Number, default: 0 },
  timestamp: { type: Number, required: true },
  timestampUTC: { type: String },
  textContent: { type: String },
  imageContent: { type: String },
  contributor: { type: String, default: "Paragon" },
  readCount: { type: Number },
});

module.exports = mongoose.model('PostMessage', postMessageSchema);