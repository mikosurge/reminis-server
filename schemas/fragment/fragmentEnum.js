const WORD_TYPE = ['n', 'v', 'adj', 'adv', 'pn', 'sfx', 'prep'];

/**
 * @description Store all abbreviations of word source, which is where the word refers to
 * @example
 * SIS: Sisters Framework
 * CAM: Cambridge Dictionary
 * OLD: Oxford Learner Dictionary
 * GGL: Google Search
 * COL: Collin Dictionary
 * TFD: The Free Dictionary
 * M-W: Merriam-Webster Dictionary
 * WKT: Wikitionary
 */
const WORD_SOURCE = ['SIS', 'CAM', 'OLD', 'GGL', 'COL', 'TFD', 'M-W', 'WKT'];