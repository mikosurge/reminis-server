const mongoose = require('mongoose');
const Schema = mongoose.Schema;


/**
 * @description A 3-way bridge to connect all shared of languages.
 * All field of languages are supposed to be Object ID.
 */
const synergyWord = new Schema({
  japanese: { type: String },
  english: { type: String },
  vietnamese: { type: String },
  origin: { type: String },
  createdAt: Date,
});

module.exports = mongoose.model('SynergyWord', synergyWord);