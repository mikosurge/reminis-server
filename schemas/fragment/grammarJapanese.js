const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const { SISTERS_FRAMEWORK } = require('../../env');

const japaneseGrammarSchema = new Schema({
  japaneseDescription: { type: String, required: true },
  englishDescription: { type: String, required: true },
  setsuzoku: String,
  examples: [{
    original: { type: String, required: true },
    englishTranslation: String
  }],
  groups: [{ type: String }],
  origin: { type: String, default: SISTERS_FRAMEWORK }
});

module.exports = mongoose.model('JapaneseGrammar', japaneseGrammarSchema);