const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const { WORD_SOURCE } = require('./fragmentEnum');
const { SISTERS_FRAMEWORK } = require('../../env');

const englishPhraseSchema = new Schema({
  phrase: { type: String, required: true },
  meaningSource: { type: String, enum: WORD_SOURCE },
  meaning: { type: String, required: true },
  examples: [{ type: String }],
  relatives: [{ type: String }],
  groups: [{ type: String }],
  alternate: String,
  origin: { type: String, default: SISTERS_FRAMEWORK }
});

module.exports = mongoose.model('EnglishPhrase', englishPhraseSchema);