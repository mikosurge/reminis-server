const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const {
  WORD_TYPE,
  WORD_SOURCE,
} = require('./fragmentEnum');
const { SISTERS_FRAMEWORK } = require('../../env');

const englishWordSchema = new Schema({
  wordType: [{ type: String, enum: WORD_TYPE, required: true }],
  word: { type: String, required: true },
  pronunciation: {
    name: { type: String },
    bre: { type: String },
  },
  meaningSource: { type: String, enum: WORD_SOURCE, default: 'GGL' },
  meaning: { type: String, required: true },
  examples: [{ type: String}],
  relatives: {
    referTo: [{ type: String }],
    synonym: [{ type: String }],
    antonym: [{ type: String }],
    similarTo: [{ type: String }],
    greaterThan: [{ type: String }],
    lesserThan: [{ type: String }],
    oppositeTo: [{ type: String }]
  },
  groups: [{ type: String }],
  alternate: String,
  origin: { type: String, default: SISTERS_FRAMEWORK },
  variant: { type: Number, default: 0 },  // In case the same word's got a different meaning
  contributor: { type: String },  // For more information
});

module.exports = mongoose.model('EnglishWord', englishWordSchema);