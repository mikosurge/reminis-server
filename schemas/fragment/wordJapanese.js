const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const { SISTERS_FRAMEWORK } = require('../../env');

const japaneseWordSchema = new Schema({
  wordType: [{ type: String }],
  word: { type: String, required: true },
  pronunciation: { type: String, required: true },
  japaneseMeaning: [{ type: String, required: true }],
  englishMeaning: [{ type: String }],
  examples: [{ type: String }],
  alternate: String,
  origin: { type: String, default: SISTERS_FRAMEWORK }
});

module.exports = mongoose.model('JapaneseWord', japaneseWordSchema);