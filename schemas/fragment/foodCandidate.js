const mongoose = require('mongoose');
const Schema = mongoose.Schema;

/**
 * @description Food candidate schema, used to store a food place after you have by chance collected it.
 * @param {String}                name - The native food name
 * @param {String}         englishName - The official english name, if any
 * @param {String}              origin - The place you found it, can be in some sites, or by references
 * @param {String}            category - Which type of food, should be food or drink or more subtypes
 * @param {Boolean}            visited - True if you have visited it
 * @param {String}              review - A short description or review about the food
 * @param {Date}             createdAt - The date that you created this entry
 */
const foodCandidate = new Schema({
  name: { type: String, required: true },
  englishName: { type: String },
  origin: { type: String, required: true },
  category: { type: String },  // should be Object type of FoodCategory
  visited: Boolean,
  review: { type: String, default: "" },
  createdAt: Date,
});

module.exports = mongoose.model('FoodCandidate', foodCandidate);