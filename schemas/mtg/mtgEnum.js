const COLOR = ['W', 'U', 'B', 'R', 'G'];
const LAYOUT = ['normal', 'split', 'flip', 'transform', 'meld', 'leveler', 'saga', 'planar', 'scheme', 'vanguard', 'token', 'double_faced_token', 'emblem', 'augment', 'host'];
const FRAME = ['1993', '1997', '2003', '2015', 'future'];
const LANGUAGE = ['en', 'es', 'fr', 'de', 'it', 'pt', 'ja', 'ko', 'ru', 'zhs', 'zht', 'he', 'la', 'grc', 'ar', 'sa', 'px'];
const FORMAT = [
  'standard',
  'modern',
  'oathbreaker',
  'constructed',
  'commander',
  'legacy',
  'vintage',
  'brawl',
  'team unified constructed',
  'block',
  'two headed giant',
  'pauper',
  'booster draft',
  'sealed deck',
  'conspiracy format',
  'team booster draft',
  'team sealed deck',
];
const RARITY = ['common', 'uncommon', 'rare', 'mythic'];
const GAME = ['arena', 'mtgo', 'paper'];