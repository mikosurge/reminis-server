const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const {
  FORMAT,
} = require('./mtgEnum');

const deckSchema = new Schema({
  name: { type: String, required: true },
  description: { type: String },
  avatar: { type: String, default: "" },
  owner: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
  format: { type: String, enum: FORMAT },
  cards: [{
    card: { type: String, ref: 'Card' },
    foil: Boolean,
    num: Number,
    isMain: Boolean,
    _id: false,
  }],
  imaginary: { type: Boolean, default: false },
  colors: { type: Array, default: [] },
  visited: { type: Number, default: 0 },
  updatedAt : Date,
});

module.exports = mongoose.model('Deck', deckSchema);