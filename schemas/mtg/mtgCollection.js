const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var collectionSchema = new Schema({
  name: { type: String, required: true},
  description: { type: String },
  owner: { type: String },
  cards: [{
    card: { type: String, ref: 'Card' },
    foil: Boolean,
    num: Number,
    _id: false
  }],
  createdAt: Date
});

module.exports = mongoose.model('Collection', collectionSchema);