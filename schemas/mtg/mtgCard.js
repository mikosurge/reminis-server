const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const {
  COLOR,
  LAYOUT,
  FRAME,
  LANGUAGE,
  RARITY,
  GAME,
} = require('./mtgEnum');


/**
 * @description Magic: The Gathering card schema, represents one card unit from scryfall.
 * It contains the card content only, while not showing the card is foil or not.
 * While most of the information can be parsed from scryfall, the current price needs to be fetched from tcgplayer.
 * @function cardSchema
 * @param {String}          cardObject - Imported from scryfall, to indicate it's a card
 * @param {String}                 _id - The scryfall ID
 * @param {String}            oracleId - The card's oracle ID, currently no idea
 * @param {Array<Number>} multiverseIds - The card's multiverse ID, which might related to other exps or games
 * @param {String}                name - The original card name
 * @param {String}         printedName - The name printed on the card's title, which might differ from original
 * @param {String}                lang - The 2-character language code
 * @param {String}                 uri - The direct URL to get the card's information
 * @param {String}         scryfallUri - The scryfall URL to get the card's information
 * @param {String}              layout - The card's layout type, one of those given in Enum
 * @param {Boolean}       highresImage - True if high resolution image for the card is provided
 * @param {Array<String>}    imageUris - URLs to all possible image resolutions of the card
 * @param {String}            manaCost - The card's mana cost, depicted at its top-right corner
 * @param {String}                 cmc - Convered mana cost, the total number of mana used to cast
 * @param {String}            typeLine - The card's full type line
 * @param {String}     printedTypeLine - The card's printed type
 * @param {String}          oracleText - The card's original description
 * @param {String}         printedText - The card's printed description
 * @param {Array<String>}       colors - All colors need to be used to cast
 * @param {Array<String>} colorIdentity - All colors in the cost and description of the card
 * @param {Object<Boolean>} legalities - Indicate if the card is available in a specific play mode
 * @param {Array<String>}        games - Show all game platform that the card is available
 * @param {Boolean}           reserved - Unknown field
 * @param {Boolean}               foil - Indicate if the card is foiled by default
 * @param {Boolean}            nonfoil - Indicate if the card is non-foiled by default
 * @param {Boolean}          oversized - Unknown field
 * @param {Boolean}              promo - Indicate if the card is from promo
 * @param {Boolean}            reprint - Indicate if the card is a reprinted version of some cards
 * @param {Boolean}          variation - Unknown field
 * @param {String}                 set - The 3-character set/expansion code that the card came from
 * @param {String}             setName - Full name of set/expansion that the card came from
 * @param {String}             setType - Indicate which type of set/expansion TODO: create set type enum
 * @param {String}              setUri - The URL that can be used to look up the set/expansion
 * @param {String}        setSearchUri - Unknown field
 * @param {String}      scryfallSetUri - The scryfall URL that can be used to look up the set/expansion
 * @param {String}          rulingsUri - The URL for card ruling/clarification
 * @param {String}     printsSearchUri - Unknown field
 * @param {String}     collectorNumber - The specific ID of a card in its set/expansion
 * @param {Boolean}            digital - Unknown field
 * @param {String}              rarity - The card's rarity, one of C/UC/R/M
 * @param {String}          flavorText - The italic text regarding the card's story, tale or quote
 * @param {String}      illustrationId - The card's illustration ID
 * @param {String}          cardBackId - Unknown field
 * @param {String}              artist - Name of the artist who draw the card's art
 * @param {String}           artistIds - The unique id of the artist
 * @param {String}         borderColor - The color of the card's border
 * @param {String}               frame - One of existing card frame type
 * @param {String}         frameEffect - Not sure until we know
 * @param {Array<String>} frameEffects - Not sure until we know
 * @param {Boolean}            fullArt - Indicate if the card is in full-art version
 * @param {Boolean}           textless - Indicate if the card has no text
 * @param {Boolean}            booster - Unknown field
 * @param {Boolean}     storySpotlight - Unknown field
 * @param {Number}          edhrecRank - Ranking of the card in Commander (EDH)
 * @param {String}              mtgoId - The card's ID on MTGO
 * @param {String}             arenaId - The card's ID on MTG Arena
 * @param {String}         tcgplayerId - The card's tcg product ID
 * @param {String}          releasedAt - The date that the card has released
 * @param {String}               power - The power index, if the card is creature card
 * @param {String}           toughness - The toughness index, if the card is creature card
 * @param {Array<Number>}       prices - scryfall prices of the card, in usd, eur or tix
 * @param {Array<String>}  relatedUris - Some related URLs for more information
 * @param {Array<String>} purchaseUris - Some related URLS for purchasing
 * @param {Array<Object<Number>>} tcgPrices - [TCG] The card's price categoried
 * @param {Date}             updatedAt - The latest data and time the card information is updated
 * @param {Array<Card>}      cardFaces - In case the card is double-faced, some information is shown here instead
 * @param {String}           watermark - The emblem that is depicted as background of the oracle text
 */
const cardSchema = new Schema({
  cardObject: { type: String, enum: ['card'], default: 'card' },
  _id: String,
  // cardId: String,
  oracleId: String,
  multiverseIds: [Number],
  name: String,
  printedName: String,
  releasedAt: String,
  lang: { type: String, enum: LANGUAGE },
  uri: String,
  scryfallUri: String,
  layout: { type: String, enum: LAYOUT },
  highresImage: Boolean,
  imageUris: {
    small: String,
    normal: String,
    large: String,
    png: String,
    artCrop: String,
    borderCrop: String
  },
  manaCost: String,
  cmc: { type: Number, min: 0 },
  typeLine: String,
  printedTypeLine: String,
  oracleText: String,
  printedText: String,
  colors: [{ type: String, enum: COLOR }],
  colorIdentity: [String],
  legalities: {
    standard: String,
    future: String,
    frontier: String,
    modern: String,
    legacy: String,
    pauper: String,
    vintage: String,
    penny: String,
    commander: String,
    duel: String,
    oldschool: String,
    brawl: String,
  },
  games: [{ type: String, enum: GAME }],
  reserved: Boolean,
  foil: Boolean,
  nonfoil: Boolean,
  oversized: Boolean,
  promo: Boolean,
  reprint: Boolean,
  variation: Boolean,
  set: String,
  setName: String,
  setType: String,
  setUri: String,
  setSearchUri: String,
  scryfallSetUri: String,
  rulingsUri: String,
  printsSearchUri: String,
  collectorNumber: String,
  digital: Boolean,
  rarity: { type: String, enum: RARITY},
  flavorText: String,
  illustrationId: String,
  cardBackId: String,
  artist: String,
  artistIds: [String],
  borderColor: String,
  frame: { type: String, enum: FRAME },
  frameEffect: String,
  frameEffects: [String],
  fullArt: Boolean,
  textless: Boolean,
  booster: Boolean,
  storySpotlight: Boolean,
  edhrecRank: Number,
  mtgoId: Number,
  arenaId: Number,
  tcgplayerId: Number,
  power: String,
  toughness: String,
  watermark: { type: String, default: null },
  prices: {
    usd: Number,
    usdFoil: Number,
    eur: Number,
    tix: Number
  },
  relatedUris: {
    gatherer: String,
    tcpplayerDecks: String,
    edhrec: String,
    mtgtop8: String
  },
  purchaseUris: {
    tcgplayer: String,
    cardmarket: String,
    cardhoarder: String
  },
  tcgPrices: {
    normal: {
      market: { type: Number, default: null},
      low: { type: Number, default: null },
      mid: { type: Number, default: null },
      high: { type: Number, default: null },
      directLow: { type: Number, default: null }
    },
    foil: {
      market: { type: Number, default: null},
      low: { type: Number, default: null },
      mid: { type: Number, default: null },
      high: { type: Number, default: null },
      directLow: { type: Number, default: null }
    }
  },
  cardFaces: [{
    object: { type: String, default: "card_face" },
    name: String,
    manaCost: String,
    typeLine: String,
    oracleText: String,
    colors: [String],
    flavorText: String,
    artist: String,
    illustrationId: String,
    imageUris: {
      small: String,
      normal: String,
      large: String,
      png: String,
      artCrop: String,
      borderCrop: String
    },
    _id: false,
  }],
  updatedAt: { type: Date, default: Date.now }
});

module.exports = mongoose.model('Card', cardSchema);