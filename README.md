# Reminis MTG API
#### Dependencies
----
- **scryfall:** The bulk data of all cards, card images in all sizes, scryfall unique ID, SVGs.
- **tcgplayer:** Mostly pricing.
- **mongodb:** No-SQL DB used to store data of users, collections, decks and cards.
- **ReactJS:** Real-time Library, developed by Facebook, to provide a smooth UI/UX.
- **express.js:** A node module used for building up REST API, which returns every response of the App.
- **axios:** A renowned JS library for HTTP requests, process-able via Promise and async/await.
#### Documentation & registration
----
##### **[scryfall](https://scryfall.com/docs/api)**
A complete data of every cards, which supports REST API itself for categorizing and filtering the card we need. It is recommended to download it's bulk data of card frequently. The pricing information from scryfall is not detailed, so that we decided to fetch from tcgplayer for every given period.
**Some of scryfall APIs:**
- Bulk data
- Catalogs
- Cards
##### **[tcgplayer](https://docs.tcgplayer.com/reference)**
A realtime database of card pricing, not only for MTG. It might take some processes before you can access its API.
It starts from sending an email to a business guy of TCGPlayer and hopefully wait for the reply, in which the access is granted with public key and private key.
**Once you've got both keys, it's time to do POST and get yourself an [access token](https://api.tcgplayer.com/token).**
***Note:*** As a crucial requirement, you need to set `Content-Type` to `application/x-www-form-urlencoded`, then provide your keys in the request body (you should refer to the raw input view, while the form data view is just for clarification):
###### Raw input view:
`grant_type=client_credentials&client_id=<public_key>&client_secret=<private_key>`
###### Form data view:
|Field|Value|
|---|---|
|grant_type|client_credentials|
|client_id|<public_key> (xxxxxxxx-xxxx-xxxx-xxxxxxxxxxxx)|
|client_secret|<private_key> (xxxxxxxx-xxxx-xxxx-xxxxxxxxxxxx)|
**If the response contains the token like below format, you've succeeded.**
```
{
  "access_token": "<token>",
  "token_type": "bearer",
  "expires_in": <Number>,
  "userName": "<public_key>",
  ".issued": "<Date>",
  ".expires": "<Date>"
}
```
Set `Content-Type` to `application/json` and `Authorization` to `bearer <token>` is adequate to make use of TCG APIs.
**Some of tcgplayer APIs:**
- Group listing (Each individual TCG game is a group).
- Group prices (Each expansion is also a group with its ID).
- Product prices (Returning the price of an individual product).
#### Functionalities
----
The Reminis server serves methods to access the parsed database, also returns the Web UI for easy communication with users. While some of these functionalities are under constructions, you can see all of the functionalities below:
##### Server side
- **User:** Create, Edit, Update, Delete
- **Collection:** Edit, Update (Each user has only one collection).
- **Deck:** Create, Edit, Update, Delete
- **Card (internal):** Import, Update
##### Client side (Web UI)
- **User:** Login, Logout, Update
- **Collection:** Browse, Filter, Sort, Navigation
- **Deck:** Browse, Create, Edit, Statistic, Delete, Filter, Sort
#### APIs
----
|Type|Path|Description|
|---|---|---|
|GET|**/mtg/:id/collections**|Returns the collection of user ___<id>___|
|GET|**/mtg/:id/collection/:cid**|Returns a specific colleciton ___<cid>___ of user ___<id>___|
|POST|**/mtg/:id/collection/:cid/mofidy**|Commit a change to collection ___<cid>___ of user ___<id>___|
|GET|**/mtg/:id/decks**|Returns all decks of user ___<id>___|
|GET|**/mtg/:id/deck/info/:did**|Returns a specific deck ___<did>___ of user ___<id>___|
|POST|**/mtg/:id/deck/modify/:did**|Commit a change to deck ___<did>___ of user ___<id>___|
|GET|**/mtg/update/tcgplayer/groups**|Update TCGPlayer groups of MTG|
|GET|**/mtg/update/tcgplayer/pricing/:gid**|Update pricings of a specific expansion ___<gid>___ of MTG, following TCGPlayer|
|GET|**/mtg/cards/brief**|Return all cards in database, in brief (some fields are omitted)|
|GET|**/mtg/cards/full**|Return the full JSON of all cards|
#### Utilities
----
Due to the need of quick update, response and backup, some functionalities are provided for specific use cases, in which you can take advantage of it in stead of thinking about another way to approach. These functionalities are all written in corresponding `js` files.

|Functionality|Description|
|---|---|
|importFromJson|Import the **bulk data** file from scryfall to **mongodb**|
|updateSingle|Update a document of one single card using its **scryfall ID**|
|updateFromScryfall|Update one or several cards by requesting to **scryfall API**|
|mergeProductsAndPricing|Request for products in a group, along with its price from ***tcgplayer***, then merge them|
|recoverCollection|Add a collection by a prior backup JSON|