const {
  TCGPLAYER_BEARER_TOKEN,
  TCGPLAYER_ENDPOINT,
} = require('../env');
const axios = require('axios');

module.exports.sleep = (ms) => {
  return new Promise(
    resolve => {
      setTimeout(() => {
        resolve();
      }, ms);
    }
  );
};

module.exports.Timer = class Timer {
  constructor(topic) {
    this.topic = topic;
  }

  start() {
    this.startTime = new Date();
  }

  end(newTopic) {
    if (newTopic) {
      this.topic = newTopic;
    }

    this.endTime = new Date();
    this.timeElapsedInSeconds = Math.round((this.endTime - this.startTime) / 1000);
    console.log(`[${this.topic}] Time elapsed: ${this.timeElapsedInSeconds}s`);
  }
};


/**
 * @description Check if every key required is present.
 * Here we will not accept either null, undefined or "" value.
 * @function checkForRequired
 * @param {Object} inputJson - The JSON Object you want to check
 * @param {Array} keys - The given keys which are required to be represent
 * @return {Array} Missing keys
 */
module.exports.checkForRequired = (inputJson, keys) => {
  const missingKeys = [];
  keys.forEach(
    key => {
      if (!inputJson[key]) {
        missingKeys.push(key);
      }
    }
  );

  return missingKeys;
};


module.exports.checkForLeastRequired = (inputJson, keys) => {
  return keys.reduce((valid, key) => valid || !!inputJson[key], false);
};

/**
 * @description Provide the default axios object for tcgplayer API.
 */
module.exports.tcgplayerApi = axios.create({
  baseURL: TCGPLAYER_ENDPOINT,
});

module.exports.tcgplayerHeaders = {
  "Content-Type": "application/json",
  "Authorization": `bearer ${TCGPLAYER_BEARER_TOKEN}`,
};