const ldap = require('ldapjs');
const AndFilter = ldap.AndFilter;
const EqualityFilter = ldap.EqualityFilter;

const BIND_CREDENTIAL_USERNAME = "han.stf@gameloft.com";
const BIND_CREDENTIAL_PASSWORD = "Gameloft@2018";

const ldapOptions = {
  bind: {
    credentials: "",
    dn: "",
  },
  maxConnection: 10,
  search: {
    class: "top",
    dn: "DC=gameloft, DC=org",
    field: "sAMAccountName",
    scope: "sub",
  },
  timeout: 1000,
  url: "ldap://han-dc01.gameloft.org:389",
  username: {
    field: "cn"
  },
};

class InvalidCredentialsError extends Error {
  constructor(user = "", ...params) {
    super(...params);

    Error.call(this, `Invalid credentials for user ${user}`);

    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, InvalidCredentialsError);
    }

    this.user = user;
    this.date = new Date();
  }
}

const login = async (email, password) => {
  let username = email.substring(0, email.indexOf('@'));
  console.log(`[ldap] username: ${username}`);

  const tryToConnect = () => {
    return new Promise(
      (resolve, reject) => {
        const { url, timeout, maxConnection } = ldapOptions;

        let client = ldap.createClient({
          url,
          timeout,
          maxConnection,
        });

        client.on('error', err => {
          console.log(`[ldap] Unexpected error: ${err}`);
          reject(err);
        });

        client.bind(BIND_CREDENTIAL_USERNAME, BIND_CREDENTIAL_PASSWORD,
          err => {
            if (err) { reject(err); }
            else { resolve(client); }
          }
        );
      }
    );
  };

  const tryToFind = client => {
    return new Promise(
      (resolve, reject) => {
        let query = {
          scope: "sub",
          filter: new AndFilter({
            filters: [
              new EqualityFilter({
                attribute: 'objectClass',
                value: ldapOptions.search.class,
              }),
              new EqualityFilter({
                attribute: ldapOptions.search.field,
                value: username,
              }),
            ],
          }),
        };

        client.search(ldapOptions.search.dn, query,
          (err, search) => {
            if (err) {
              console.log(`[ldap] Error when trying to search for ${username}: ${err}`);
              reject(err);
            } else {
              const searchEntryListener = entry => {
                removeListeners(search);
                resolve(entry);
              };

              const endListener = () => {
                console.log(`[ldap] End signal detected`);
                removeListeners(search);
                reject(new InvalidCredentialsError(username));
              };

              const errorListener = err => {
                console.log(`[ldap] Error after searching for ${username}: ${err}`);
                removeListeners(search);
                reject(err);
              }

              const removeListeners = search => {
                search.removeListener('searchEntry', searchEntryListener);
                search.removeListener('end', endListener);
                search.removeListener('error', errorListener);
              };

              search.on('searchEntry', searchEntryListener);
              search.on('end', endListener);
              search.on('error', errorListener);
            }
          }
        );
      }
    );
  };

  const tryToBind = (client, entry) => {
    return new Promise(
      (resolve, reject) => {
        client.bind(entry.object.dn, password,
          err => {
            client.unbind();
            if (err) {
              console.log(`[ldap] Error when trying to bind ${entry.object.dn}`);
              reject(new InvalidCredentialsError(username));
            } else {
              resolve(entry.object);
            }
          }
        )
      }
    );
  };

  let client = await tryToConnect();
  let entry = await tryToFind(client);
  let user = await tryToBind(client, entry);
  return user;
};

module.exports = {
  InvalidCredentialsError,
  login,
};