class ReminisError extends Error {
  constructor(message) {
    super(message);
    this.name = this.constructor.name;
    if (typeof Error.captureStackTrace === 'function') {
      Error.captureStackTrace(this, this.constructor);
    } else {
      this.stack = (new Error(message)).stack;
    }
  }
}

const AUTHCODE = {
  WRONG_USERNAME: 1,
  WRONG_PASSWORD: 2,
  MISSING_FIELDS: 3,
  TOKEN_EXPIRED: 4,
  properties: {
    1: { message: 'The username is not correct', step: 'Login' },
    2: { message: 'The password is not match', step: 'Login' },
    3: { message: 'Missing fields: ', step: 'Signup' },
    4: { message: 'Token expired', step: 'Guard' },
  },
};

class AuthenticationError extends ReminisError {
  constructor(code, detail = null) {
    // super(message);
    super(AUTHCODE.properties[code].message + (detail || ''));
    this.code = code;
    this.name = this.constructor.name;
    this.step = AUTHCODE.properties[code].step;
  }

  dbg() {
    console.log(this.message);
    console.log(this.stack);
  }
}

const DBCODE = {
  NOT_EXIST: 1,
  DUPLICATE: 2,
  OPERATION_FAILED: 3,
  properties: {
    1: { message: 'Not exist: ' },
    2: { message: 'Duplicated: ' },
    3: { message: 'Operation failed: '},
  }
};

class DatabaseError extends ReminisError {
  constructor(code, detail = null) {
    super(DBCODE.properties[code].message + (detail || ''));
    this.code = code;
    this.name = this.constructor.name;
  }
}

module.exports = {
  AUTHCODE,
  DBCODE,
  ReminisError,
  AuthenticationError,
  DatabaseError,
};