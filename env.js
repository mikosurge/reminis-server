module.exports.JWT_SECRET = "paragon_the_princess_of_atonement";
module.exports.SISTERS_FRAMEWORK = "Sisters";
module.exports.DATABASE_NAME = "mtg_test";
module.exports.MONGODB_URI = "mongodb://localhost:27017";

module.exports.TCGPLAYER_BEARER_TOKEN = "nxrRmEyBfqEu6eMOc1izXL3uQJVdz9CTonCxUKGxISr2Q1HTY4HA1RmVfx9mCz70vQwZJOq0GRhbE43X9HNNti7ebjhBBgsUkRgPNViDqmc1lMOQJ81_7Dhca4Lx73vuM-ZAY6zupOUdxwxPhQeWzKDkTwyWKlUl8VrzD9wKb6PNZzWdsu-r_77HpkCq8lxVOT4JKJ0W_RodLREOrlmnLanNDZakW4vcpUBirxt2ur1tLNYh4m7FEkNEzgEwIc6Z3zzJrGlmqGYM4n6dBN98QvuFZ9FRYf8bL3JMnbSLeQcHekWE0_Vwh-yWy4gWRpuImotbwQ";
module.exports.TCGPLAYER_RESPONSE_LIMIT = 100;
module.exports.TCGPLAYER_ENDPOINT = "https://api.tcgplayer.com/v1.27.0";
module.exports.TCGPLAYER_GROUP_MTG = 1;

module.exports.ERROR_NO_AUTHENTICATION = "No authentication!";