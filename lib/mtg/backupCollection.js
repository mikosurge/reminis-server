const fs = require('fs');
const mongoose = require('mongoose');

const Collection = require('../../schemas/mtg/mtgCollection');
const { MONGODB_URI, DATABASE_NAME } = require('../../env');


/**
 * @description Read the backup Json of a collection, then try to add it to mongodb
 * The collection and cards are following MTG schemas.
 * @function backupCollectionAsync
 * @param {ObjectId} collectionId - The id of target collection in mongodb
 */
const backupCollectionAsync = (collectionId, backupJsonPath) => {
  return new Promise (
    resolve => {
      console.log(`Update collection with ID = ${collectionId}`);
      const collectionRaw = fs.readFileSync(backupJsonPath);
      const collectionJson = JSON.parse(collectionRaw);
      const cards = collectionJson.collection.cards;

      let numberOfProcessed = 0;
      let numberOfItems = cards.length;

      cards.forEach(
        item => {
          Collection.updateOne(
            {
              _id: mongoose.Types.ObjectId(collectionId),
            },
            {
              $push: {
                cards: {
                  card: item.card._id,
                  foil: item.foil,
                  num: item.num,
                }
              }
            }
          )
            .then(
              () => console.log(`Processed ${item.card.name}`)
            )
            .catch(
              err => console.log(err)
            )
            .finally(
              () => {
                numberOfProcessed += 1;
                if (numberOfProcessed === numberOfItems) {
                  console.log(`Processed ${numberOfItems} items`);
                  resolve();
                }
              }
            );
        }
      );
    }
  )
};

if (require.main === module) {
  console.log("Enter the collection ID:");
  process.stdin.setEncoding('utf-8').on('data', function (data) {
    if (data === 'exit\r\n' || data === 'exit\n') {
      console.log("Exiting...");
      process.exit();
    } else {
      console.log(`User inputted: ${data.trim()}, updating collection`);
      let databasePath = `${MONGODB_URI}/${DATABASE_NAME}`;
      mongoose.connect(databasePath,
        {
          useCreateIndex: true,
          useNewUrlParser: true,
          useUnifiedTopology: true,
        })
        .then(
          () => {
            console.log(`Connected to ${databasePath}`);
            return backupCollectionAsync(data.trim());
          }
          )
        .then(
          () => {
            console.log(`Done backing up collection ${data.trim()}`);
            mongoose.connection.close();
          }
        )
        .catch(
          error => console.log(`Connection to ${databasePath} failed! Got error ${error}`)
        )
    }
  });
};

module.exports = {
  backupCollectionAsync,
};