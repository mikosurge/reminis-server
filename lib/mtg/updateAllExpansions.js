const fs = require('fs');
const { Timer } = require('../../utils');

const mongoose = require('mongoose');
const Card = require('../../schemas/mtg/mtgCard');

const {
  TCGPLAYER_RESPONSE_LIMIT,
  MONGODB_URI,
  DATABASE_NAME,
} = require('../../env');

const JAPANESE_ART = "JP Alternate Art";
const PATH_TCGPLAYER_GROUPS = `${__dirname}/../../lazyres/tcg_mtg_groups.json`;
const groupsJson = JSON.parse(fs.readFileSync(PATH_TCGPLAYER_GROUPS, { encoding: 'utf8' }));

const {
  tcgplayerApi,
  tcgplayerHeaders,
} = require('../../utils');

const existingExpansions = [
  "Throne of Eldraine",
  "Commander 2019",
  "Core Set 2020",
  "Modern Horizons",
  "War of the Spark",
  "Ravnica Allegiance",
  "Guilds of Ravnica",
  "Core Set 2019",
  "Battlebond",
  "Dominaria",
  "Rivals of Ixalan",
  "Ixalan",
  "Hour of Devastation",
  "Amonkhet",
  "Aether Revolt",
  "Kaladesh",
  "Eldritch Moon",
  "Shadows over Innistrad",
  "Oath of the Gatewatch",
  "Battle for Zendikar",
  "Magic Origins",
  "Modern Masters 2015",
  "Dragons of Tarkir",
  "Fate Reforged",
  "Khans of Tarkir",
  "Magic 2015",
  "Journey Into Nyx",
  "Born of the Gods",
  "Theros",
  "Magic 2014",
  "Dragon's Maze",
  "Gatecrash",
  "Return to Ravnica",
  "Magic 2013",
  "Avacyn Restored",
  "Dark Ascension",
  "Innistrad",
  "Magic 2012",
  "New Phyrexia",
  "Mirrodin Besieged",
  "Scars of Mirrodin",
  "Magic 2011",
  "Rise of the Eldrazi",
  "Worldwake",
  "Zendikar",
  "Magic 2010",
  "10th Edition",
  "Weatherlight",
  "Alliances"
];


/**
 * @description Update all cards of the product list to database.
 * @function updateCardsAsync
 * @param {Array} products - List of products merged with its price from api.tcgplayer.com
 * @param {String} set - An abbreviation of a MTG expansion. For example: M19, GRN, RNA, WAR, ELD
 * @return {Array} - The list of errors, if any
 */
const updateCardsAsync = (products, set) => {
  return new Promise(
    resolve => {
      let numberOfItems = products.length;
      let numberOfProcessed = 0;
      let logs = [];

      console.log(`Updating set ${set}...`);
      products.forEach(
        item => {
          let captionPos = item.name.indexOf("(");
          let tokenPos = item.name.indexOf("Token");
          let checklistPos = item.name.indexOf("Checklist");
          let emblemPos = item.name.indexOf("Emblem");
          let caption = "";  // Supposed to be the text in brackets
          let queries = {};
          let actualName = item.name;
          let tokenSet = `t${set.toLowerCase()}`;
          let lowerCaseSet = set.toLowerCase();

          if (emblemPos !== -1) {
            // Expect that emblem cards are in the format of `Emblem - <actual name>`
            // On An Adventure Emblem
            console.log(`Detected Emblem in ${item.name}`);
            if (item.name.split("-").length <= 1) {
              actualName = item.name.substring(0, emblemPos - 1);
            } else {
              actualName = item.name.split("-")[1].trim();
            }
            let queryName = `${actualName} Emblem`;
            queries = {
              name: {
                $regex: queryName,
              },
              set: tokenSet,
              lang: "en",
            };
          } else if (checklistPos !== -1) {
            console.log(`Detected Checklist card in ${item.name}`);
            queries = {
              name: {
                $regex: "Checklist",
              },
              set: tokenSet,
              lang: "en",
            };

            if (captionPos !== -1) {
              // This might be CH1 or CH2 in brackets
              caption = item.name.substring(captionPos + 1, item.name.indexOf(")")).toUpperCase();
              if (caption === "CH1" || caption === "CH2") {
                console.log(`This item might be in Shadows over Innistrad exp.`);
                queries = {
                  name: {
                    $regex: "Checklist",
                  },
                  set: tokenSet,
                  lang: "en",
                  collectorNumber: caption,
                };
              }
            }  // if (captionPos !== -1)
          } else {  // if (checklistPos !== -1)
            if (tokenPos !== -1) {
              // Saproling Token | Elemental Token (002)
              console.log(`Detected ${item.name} as a token`);
              actualName = item.name.substring(0, tokenPos - 1);
              queries = {
                name: {
                  $regex: actualName,
                },
                set: tokenSet,
              };
            } else if (captionPos !== -1) {
              // It might be a land with its own ID in caption, or a description
              console.log(`Detected ${item.name} has a caption`);
              actualName = item.name.substring(0, captionPos - 1);
              caption = item.name.substring(captionPos + 1, item.name.indexOf(")"));

              // (4 Soldiers) or (NotANumber) should be detected
              if (isNaN(parseInt(caption)) || caption.indexOf(" ") !== -1) {
                // Not a land, might be a description of JP Alternate Art
                if (caption === JAPANESE_ART) {
                  queries = {
                    name: {
                      $regex: actualName,
                    },
                    set: lowerCaseSet,
                    lang: "ja",
                  };
                } else {
                  queries = {
                    name: {
                      $regex: actualName,
                    },
                    set: lowerCaseSet,
                    lang: "en",
                  };
                }
              } else {
                // Some basic lands
                queries = {
                  name: {
                    $regex: actualName,
                  },
                  set: lowerCaseSet,
                  lang: "en",
                  collectorNumber: parseInt(caption),
                };
              }
            } else {
              // Not a caption, not a token nor a checklist
              queries = {
                name: {
                  $regex: item.name,
                },
                set: lowerCaseSet,
                lang: "en",
              };
            }
          }  // end of if/else

          let updateArguments = {
            $set: {
              tcgPrices: item.tcgPrices,
              timeUpdated: new Date().toISOString(),
            },
          };

          // Sometimes the query is just simple like this
          queries = {
            tcgplayerId: item.productId,
          };

          Card.updateOne(queries, updateArguments)
            .then(
              result => {
                if (result.nModified === 0) {
                  console.log(`Item ${actualName} (${caption}) was not updated!`);
                }
              }
            )
            .catch(
              err => logs.push(`Failed to update ${actualName} (${caption}), got error ${JSON.stringify(err)}`)
            )
            .finally(
              () => {
                numberOfProcessed += 1;
                if (numberOfProcessed === numberOfItems) {
                  console.log(`Processed ${numberOfItems} items of set ${set}. Returning...`);
                  resolve(logs);
                }
              }
            );  // Card.updateOne.finally
        }  // item
      );  // forEach
    }
  );
};


/**
 * @description Get all products and pricing of one expansion of MTG, merge to one JSON and update database.
 * Since products request return only 100 items for a chunk, if there are more items after the first request,
 * this method will fire a sufficient amount of additional requests to get remaining products.
 * @param {String} groupId - The ID of an expansion in MTG, based on api.tcgplayer.com
 * @return {Array} - The array of error logs, if any
 */
const getGroupPriceAndUpdateDatabaseAsync = async groupId => {
  let mandatoryTimer = new Timer("MANDATORY");
  mandatoryTimer.start();

  let mandatoryUris = [
    `/catalog/products?groupId=${groupId}&limit=${TCGPLAYER_RESPONSE_LIMIT}`,  // Get all products
    `/pricing/group/${groupId}`  // Get all pricing
  ];

  let mandatoryResponses = await Promise.all(
    mandatoryUris.map(
      uri => {
        return tcgplayerApi.get(uri,
          {
            headers: tcgplayerHeaders
          })
          .then(
            response => {
              return {
                ...response,
                responseStatus: 'resolved',
              };
            },
            error => {
              return {
                ...error,
                responseStatus: 'rejected',
              };
            }
          );  // then
      }  // uri
    )  // map
  );

  mandatoryTimer.end();

  mandatoryResponses.forEach(
    result => {
      if (result.responseStatus === 'rejected') {
        throw new Error("Mandatory request failed!");
      }
    }
  );

  let allProducts = [];
  let allPrices = [];

  // Get group products
  let response = mandatoryResponses[0];  // Product response
  let totalItems = response.data.totalItems;
  let numberOfResults = response.data.results.length;
  allProducts.push(...response.data.results);

  if (numberOfResults < totalItems) {
    let remainingUris = [];
    let retries = 1;
    let remainingItems = totalItems - numberOfResults;
    while (remainingItems > 0) {
      remainingUris.push(`/catalog/products/?groupId=${groupId}&offset=${retries * TCGPLAYER_RESPONSE_LIMIT}&limit=${TCGPLAYER_RESPONSE_LIMIT}`);
      remainingItems -= response.data.results.length;
      retries += 1;
    }

    let remainingResponses = await Promise.all(
      remainingUris.map(
        uri => {
          return tcgplayerApi.get(uri, 
            {
              headers: tcgplayerHeaders,
            })
            .then(
              response => {
                return {
                  ...response,
                  responseStatus: 'resolved',
                };
              },
              error => {
                return {
                  ...error,
                  responseStatus: 'rejected',
                };
              }
            );  // then
        }  // uri
      )  // map
    );

    console.log(`All remaining requests completed!`);
    remainingResponses.forEach(
      result => {
        if (result.responseStatus === 'resolved') {
          console.log(`Got ${result.data.results.length} from a remaining request`);
          allProducts.push(...result.data.results);
        } else if (result.responseStatus === 'rejected') {
          console.log(`One of the remaining request got rejected`);
        }
      }  // result
    );
  }  // if (numberOfResults < totalItems)

  mandatoryTimer.end("REMAINING");

  fs.writeFile(`${__dirname}/../../lazyres/tcg_mtg_${groupId}_products.json`, JSON.stringify(allProducts, null, 2),
    err => {
      if (err) throw err;
      console.log(`Done writing products of group ${groupId} to tcg_mtg_${groupId}_products.json`);
    }
  );

  response = mandatoryResponses[1];  // Pricing response
  allPrices.push(...response.data.results);

  allPrices.push(...response.data.results);

  fs.writeFile(`${__dirname}/../../lazyres/tcg_mtg_${groupId}_pricing.json`, JSON.stringify(allPrices, null, 2),
    (err) => {
      if (err) throw err;
      console.log(`Done writing pricing of group ${groupId} to tcg_mtg_${groupId}_pricing.json`);
    }
  );

  const productsBareId = allProducts.map(
    product => product.productId
  );

  const pricesBareId = allPrices.map(
    price => price.productId
  );

  allProducts.forEach(
    product => {
      product.tcgPrices = { };
    }
  );

  for (let i = 0; i < pricesBareId.length; i++) {
    let item = pricesBareId[i];
    let pos = productsBareId.indexOf(item);

    if (pos !== -1) {
      if (allPrices[i].subTypeName === "Normal") {
        allProducts[pos].tcgPrices.normal = {
          market: allPrices[i].marketPrice,
          low: allPrices[i].lowPrice,
          mid: allPrices[i].midPrice,
          high: allPrices[i].highPrice,
          directLow: allPrices[i].directLowPrice,
        };
      } else if (allPrices[i].subTypeName == "Foil") {
        allProducts[pos].tcgPrices.foil = {
          market: allPrices[i].marketPrice,
          low: allPrices[i].lowPrice,
          mid: allPrices[i].midPrice,
          high: allPrices[i].highPrice,
          directLow: allPrices[i].directLowPrice,
        };
      }  // else if
    }
  }

  mandatoryTimer.end("MERGE");

  fs.writeFile(`${__dirname}/../../lazyres/tcg_mtg_${groupId}_merged.json`, JSON.stringify(allProducts, null, 2),
    (err) => {
      if (err) throw err;
      console.log(`Done writing merged JSON of group ${groupId} to tcg_mtg_${groupId}_merged.json`);
    }
  );

  let rightGroup = groupsJson.filter(
    (group) => {
      return group.groupId === parseInt(groupId);
    }
  );
  let logs = await updateCardsAsync(allProducts, rightGroup[0].abbreviation);
  mandatoryTimer.end("DATABASE");
  return logs;
};


const updateAllExpansionsPricingAsync = async (expansions) => {
  const allExpansionsJson = JSON.parse(fs.readFileSync(PATH_TCGPLAYER_GROUPS));
  let existingExpansionsFull = allExpansionsJson.filter(
    expansion => expansions.includes(expansion.name.split("(")[0].trim())
  );

  for (let i = 0; i < existingExpansionsFull.length; i++) {
    let expansion = existingExpansionsFull[i];
    await getGroupPriceAndUpdateDatabaseAsync(expansion.groupId.toString());
  };
};


if (require.main === module) {
  let databasePath = `${MONGODB_URI}/${DATABASE_NAME}`;
  mongoose.connect(databasePath,
    {
      useCreateIndex: true,
      useNewUrlParser: true,
      useUnifiedTopology: true,
    })
    .then(
      () => {
        console.log(`Connected to database ${DATABASE_NAME}`);
        console.log(`Doing update pricing for all expansions...`);
        return updateAllExpansionsPricingAsync(existingExpansions);
      }
    )
    .then(
      () => {
        console.log(`Done updating pricing for all expansions...`);
        mongoose.connection.close();
      }
    )
    .catch(
      err => console.log(`Got an error: ${err}`)
    );
};

module.exports = {
  updateCardsAsync,
  getGroupPriceAndUpdateDatabaseAsync,
  updateAllExpansionsPricingAsync,
};