const fs = require('fs');
const mongoose = require('mongoose');
const Card = require('../../schemas/mtg/mtgCard');
const { MONGODB_URI, DATABASE_NAME } = require('../../env');

const PATH_BULK_DATA = './scryfall-default-cards.json';
const CHUNK_SIZE = 1000;


/**
 * @description Check if the given string is promo index, which has a form of '<digits><character>' like '239s'.
 * @function isPromoIndex
 */
const isPromoIndex = index => {
  return /^\d+[\D]$/.test(index);
};


/**
 * @description Check if the given set is promo.
 * @function isPromoSet
 */
const isPromoSet = set => {
  if (set.length > 3) {
    if (set.startsWith('t') && set.length === 4) {
      return false;
    }
    return true;
  }
  return false;
};


const determineValue = (node, key, defaultValue) => {
  if (!node) {
    return defaultValue;
  }

  if (!node[key[0]]) {
    return defaultValue;
  } else {
    return node[key[0]];
  }
};


/**
 * @description Save cards to collection asynchronously.
 * @function saveCardAsync
 */
const saveCardAsync = cards => {
  return new Promise(
    resolve => {
      let numberOfCardsPushed = 0;
      for (let i = 0; i < cards.length; i++) {
        let _c = cards[i];
        let card = new Card({
          cardObject:        _c.object,
          _id:               _c.id,
          // cardId:            _c.id,
          oracleId:          _c.oracle_id,
          multiverseIds:     _c.multiverse_ids,
          name:              _c.name,
          printedName:       _c.printed_name,
          lang:              _c.lang,
          uri:               _c.uri,
          scryfallUri:       _c.scryfall_uri,
          layout:            _c.layout,
          highresImage:      _c.highres_image,
          imageUris: {
            small:           determineValue(_c.image_uris, ['small'], null),
            normal:          determineValue(_c.image_uris, ['normal'], null),
            large:           determineValue(_c.image_uris, ['large'], null),
            png:             determineValue(_c.image_uris, ['png'], null),
            artCrop:         determineValue(_c.image_uris, ['art_crop'], null),
            borderCrop:      determineValue(_c.image_uris, ['border_crop'], null),
          },
          manaCost:          _c.mana_cost,
          cmc:               _c.cmc,
          typeLine:          _c.type_line,
          printedTypeLine:   _c.printed_type_line,
          oracleText:        _c.oracle_text,
          printedText:       _c.printed_text,
          colors:            _c.colors,
          colorIdentity:     _c.color_identity,
          legalities: {
            standard:        determineValue(_c.legalities, ['standard'], null),
            future:          determineValue(_c.legalities, ['future'], null),
            frontier:        determineValue(_c.legalities, ['frontier'], null),
            modern:          determineValue(_c.legalities, ['modern'], null),
            legacy:          determineValue(_c.legalities, ['legacy'], null),
            pauper:          determineValue(_c.legalities, ['pauper'], null),
            vintage:         determineValue(_c.legalities, ['vintage'], null),
            penny:           determineValue(_c.legalities, ['penny'], null),
            commander:       determineValue(_c.legalities, ['commander'], null),
            duel:            determineValue(_c.legalities, ['duel'], null),
            oldschool:       determineValue(_c.legalities, ['oldschool'], null),
            brawl:           determineValue(_c.legalities, ['brawl'], null),
          },
          games:             _c.games,
          reserved:          _c.reserved,
          foil:              _c.foil,
          nonfoil:           _c.nonfoil,
          oversized:         _c.oversized,
          promo:             _c.promo,
          reprint:           _c.reprint,
          variation:         _c.variation,
          set:               _c.set,
          setName:           _c.set_name,
          setType:           _c.set_type,
          setUri:            _c.set_uri,
          setSearchUri:      _c.set_search_uri,
          scryfallSetUri:    _c.scryfall_set_uri,
          rulingsUri:        _c.rulings_uri,
          printsSearchUri:   _c.prints_search_uri,
          collectorNumber:   _c.collector_number,
          digital:           _c.digital,
          rarity:            _c.rarity,
          flavorText:        _c.flavor_text,
          illustrationId:    _c.illustration_id,
          cardBackId:        _c.card_back_id,
          artist:            _c.artist,
          artistIds:         _c.artist_ids,
          borderColor:       _c.border_color,
          frame:             _c.frame,
          frameEffect:       _c.frameEffect,
          frameEffects:      _c.frameEffects,
          fullArt:           _c.full_art,
          textless:          _c.textless,
          booster:           _c.booster,
          storySpotlight:    _c.story_spotlight,
          edhrecRank:        _c.edhrec_rank,
          mtgoId:            _c.mtgo_id,
          arenaId:           _c.arena_id,
          tcgplayerId:       _c.tcgplayer_id,
          releasedAt:        _c.released_at,
          power:             _c.power,
          toughness:         _c.toughness,
          watermark:         _c.watermark,
          prices: {
            usd:             determineValue(_c.prices, ['usd'], null),
            usdFoil:         determineValue(_c.prices, ['usd_foil'], null),
            eur:             determineValue(_c.prices, ['eur'], null),
            tix:             determineValue(_c.prices, ['tix'], null),
          },
          relatedUris: {
            gatherer:        determineValue(_c.related_uris, ['gatherer'], null),
            tcgplayerDecks:  determineValue(_c.related_uris, ['tcgplayer_decks'], null),
            edhrec:          determineValue(_c.related_uris, ['edhrec'], null),
            mtgtop8:         determineValue(_c.related_uris, ['mtgtop8'], null),
          },
          purchaseUris: {
            tcgplayer:       determineValue(_c.purchase_uris, ['tcgplayer'], null),
            cardmarket:      determineValue(_c.purchase_uris, ['cardmarket'], null),
            cardhoarder:     determineValue(_c.purchase_uris, ['cardhoarder'], null),
          },
          cardFaces: _c.card_faces ? _c.card_faces.map(
            face => ({
              object:        face.object,
              name:          face.name,
              manaCost:      face.mana_cost,
              cmc:           face.cmc,
              typeLine:      face.type_line,
              oracleText:    face.oracle_text,
              colors:        face.colors,
              flavorText:    face.flavor_text,
              artist:        face.artist,
              artistId:      face.artist_id,
              illustrationId:face.illustration_id,
              imageUris: {
                small:       determineValue(face.image_uris, ['small'], null),
                normal:      determineValue(face.image_uris, ['normal'], null),
                large:       determineValue(face.image_uris, ['large'], null),
                png:         determineValue(face.image_uris, ['png'], null),
                artCrop:     determineValue(face.image_uris, ['art_crop'], null),
                borderCrop:  determineValue(face.image_uris, ['border_crop'], null),
              }
            })
          ) : null,
          updatedAt:       new Date().toISOString(),
        }
      );

      card.save()
        .then(
          result => console.log(`Saved card ${result.name}`)
        )
        .catch(
          err => console.log(`Error while saving ${card.name}: ${err}`)
        )
        .finally(
          () => {
            numberOfCardsPushed += 1;
            if (numberOfCardsPushed === cards.length) {
              resolve(numberOfCardsPushed);
            }
          }
        );
    }
  });
}

const importFromBulkDataAsync = async filename => {
  let contentRaw = fs.readFileSync(filename);
  let contentJson = JSON.parse(contentRaw);
  let numberOfCards = contentJson.length;
  console.log(`Processing ${numberOfCards}`);

  let times = 0;
  let numberOfProcessing = 0;
  let processingCards = [];
  let totalCardsPushed = 0;

  while (times * CHUNK_SIZE < numberOfCards) {
    processingCards = [];
    if ((times + 1) * CHUNK_SIZE < numberOfCards) {
      processingCards = contentJson.slice(times * CHUNK_SIZE, (times + 1) * CHUNK_SIZE);
      numberOfProcessing = CHUNK_SIZE;
    } else {
      processingCards = contentJson.slice(times * CHUNK_SIZE);
      numberOfProcessing = numberOfCards = times * CHUNK_SIZE;
    }

    console.log(`Processing ${numberOfProcessing} from index ${times * CHUNK_SIZE} to ${(times + 1) * CHUNK_SIZE}`);
    processingCards = processingCards.filter(
      card => (!isPromoSet(card.set) && !isPromoIndex(card.collectorNumber))
    );

    // Save all cards in one block of CHUNK_SIZE
    totalCardsPushed += await saveCardAsync(processingCards);
    // Time increment
    times++;
  }

  return totalCardsPushed;
};

if (require.main === module) {
  let databasePath = `${MONGODB_URI}/${DATABASE_NAME}`;
  mongoose.connect(databasePath,
    {
      useCreateIndex: true,
      useNewUrlParser: true,
      useUnifiedTopology: true,
    })
    .then(
      () => {
        console.log(`Connected to ${databasePath}`);
        return importFromBulkDataAsync(PATH_BULK_DATA);
      }
    )
    .then(
      total => {
        console.log(`Total card pushed: ${total}`);
        mongoose.connection.close();
      }
    )
    .catch(
      error => console.log(`Connection to ${databasePath} failed! Got error ${error}`)
    );
};

module.exports = {
  importFromBulkDataAsync,
  saveCardAsync,
};