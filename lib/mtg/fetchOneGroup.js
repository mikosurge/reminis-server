const fs = require('fs');
const { tcgplayerApi, tcgplayerHeaders } = require('../../utils');
const { TCGPLAYER_GROUP_MTG, TCGPLAYER_RESPONSE_LIMIT } = require('../../env');
const PATH_TCGPLAYER_GROUPS = `${__dirname}/../../lazyres/tcg_mtg_groups.json`;

const getGroupPricingAsync = async groupId => {
  const response = await tcgplayerApi.get(`/pricing/group/${grouupId}`, {
    headers: tcgplayerHeaders,
  });

  if (response.data.results) {
    fs.writeFileSync(`${__dirname}/../../lazyres/tcg_mtg_${groupId}_pricing.json`, JSON.stringify(response.data.results, null, 2));
  }

  return true;
};


/**
 * @description Return all groups of MTG via api.tcpplayer.com.
 * @function getAllGroups
 * @return {Array} The ArrayList containing all groups
 */
const getAllGroupsAsync = async () => {
  console.log(`Waiting for the first group request to be completed!`);
  let response = await tcgplayerApi.get(`/catalog/categories/${TCGPLAYER_GROUP_MTG}/groups?limit=${TCGPLAYER_RESPONSE_LIMIT}`, {
    headers: tcgplayerHeaders
  });

  let totalItems = response.data.totalItems;
  let numberOfResults = response.data.results.length;
  let allGroups = [...response.data.results];

  if (numberOfResults < totalItems) {
    console.log(`Fetching more items (${numberOfResults} < ${totalItems})`);
    let retries = 1;
    let remainingItems = totalItems - numberOfResults;
    while(remainingItems) {
      console.log(`Fetching items from ${retries * TCGPLAYER_RESPONSE_LIMIT} to ${Math.min((retries + 1) * TCGPLAYER_RESPONSE_LIMIT, totalItems)}`);
      response = await tcgplayerApi.get(`/catalog/categories/${TCGPLAYER_GROUP_MTG}/groups?offset=${retries * 100}&limit=${TCGPLAYER_RESPONSE_LIMIT}`, {
        headers: tcgplayerHeaders,
      });
      allGroups.push(...response.data.results);
      remainingItems -= response.data.results.length;
      retries += 1;
    }
  }

  fs.writeFileSync(PATH_TCGPLAYER_GROUPS, JSON.stringify(allGroups, null, 2));
  return allGroups;
};


if (require.main === module) {
  console.log("Enter the tcgplayer group ID:");
  process.stdin.setEncoding('utf-8').on('data', function (data) {
    if (data === 'exit\r\n' || data === 'exit\n') {
      console.log("Exiting...");
      process.exit();
    } else {
      console.log(`Updating tcg group ${data.trim()}`);
      getGroupPricingAsync(data.trim())
        .then(
          () => {
            console.log(`Updated tcg group ${data.trim()}`);
          }
        )
    }
  });
};

module.exports = {
  getGroupPricingAsync,
  getAllGroupsAsync,
};