/**
 * @description Centralize all functionalities that MTG offers.
 * @member {Function} getGroupPricingAsync - [fetchOneGroup] Get the pricing of a particular group on tcgplayer
 * @member {Function} getAllGroups - [fetchOneGroup] Get all tcgplayer groups and update the group JSON in resource folder
 * @member {Function} backupCollectionAsync - [backupCollection] Receive collection ID and its JSON and try to add it again to the database
 * @member {Function} updateCardsAsync - [updateAllExpansions] Update a list of cards to database
 * @member {Function} getGroupPriceAndUpdateDatabaseAsync - [updateAllExpansions] Get all products and pricing of one expansion of MTG, merge to one JSON and update database
 * @member {Function} updateAllExpansionsPricingAsync - [updateAllExpansions] Process all given expansions, update all pricing and add to the databse
 * @member {Function} importFromBulkDataAsync - [importFromBulkData] Read from the bulk data and add all available cards to the database
 * @member {Function} saveCardAsync - [importFromBulkData] Save a card (can be new) to the database
 */
module.exports = {
  ...require('./backupCollection'),
  ...require('./fetchOneGroup'),
  ...require('./importFromBulkData'),
  ...require('./updateAllExpansions'),
};